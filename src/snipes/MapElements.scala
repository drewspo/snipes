

package snipes

trait MapElements {
  def canPass(c:Character):Boolean 
  def createPassable():MapElement.ElementType
}