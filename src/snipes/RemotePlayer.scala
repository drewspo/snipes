

package snipes

@remote trait RemotePlayer {
  def passable:Passables.PassableCharacter
  def move(dx:Int, dy:Int):Unit  
  def upPressed():Unit = ???
  def downPressed():Unit = ???
  def leftPressed():Unit = ???
  def rightPressed():Unit = ???
}