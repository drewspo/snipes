

package snipes

class Enemy(charType: Int, private var mX: Int, private var mY: Int) extends Character {
  private var enemyNum = 0
  def changeNum(newNum: Int): Unit = {
    enemyNum = newNum
  }
  private var _alive = true
  def alive = _alive
  def enemyDead = {
    _alive = false
  }
  def x: Int = mX
  def y: Int = mY
  def eSpeed = 4
  def cType: Int = 2
  def level: Level = ???
  def move(dx: Int, dy: Int): Unit = {
    mX += dx
    mY += dy
  }
  def update: Unit = ???
  def passable: Passables.PassableCharacter = {
    new Passables.PassableCharacter(mX, mY, cType)
  }
  val r = scala.util.Random
  var dir = r.nextInt(4)
  def moveEnemy(maze: Array[Array[Int]], bullets: Array[Passables.PassableItem]): Boolean = {
    checkDead(bullets)
    checkIntersect(maze)
    dir match {
      case 0 => {
        if (rightClear) {
          move(eSpeed, 0)
          randomize
        } else {
          dir = r.nextInt(4)
        }
      }
      case 1 => {
        if (leftClear) {
          move(-1 * eSpeed, 0)
          randomize
        } else {
          dir = r.nextInt(4)
        }
      }
      case 2 => {
        if (upClear) {
          move(0, -1 * eSpeed)
          randomize
        } else {
          dir = r.nextInt(4)
        }
      }
      case 3 => {
        if (downClear) {
          move(0, eSpeed)
          randomize
        } else {
          dir = r.nextInt(4)
        }
      }
    }
    clearAll
    if (!alive) {
      true
    } else {
      false
    }
  }
  val z = scala.util.Random
  def randomize = {
    if (z.nextInt(10) == 9) {
      dir = r.nextInt(4)
    }
  }
  val cellWidth = 16
  private var rightClear: Boolean = true
  private var leftClear: Boolean = true
  private var upClear: Boolean = true
  private var downClear: Boolean = true
  def clearAll(): Unit = {
    rightClear = true
    leftClear = true
    upClear = true
    downClear = true
  }
  def checkIntersect(maze: Array[Array[Int]]): Unit = {
    for (i <- 0 until maze.length; j <- 0 until maze(0).length) {
      if (maze(i)(j) != 0) {
        if (Math.abs(this.x - j * cellWidth - eSpeed) < cellWidth && Math.abs(i * cellWidth - this.y) < cellWidth) leftClear = false
        if (Math.abs(j * cellWidth - this.x - eSpeed) < cellWidth && Math.abs(i * cellWidth - this.y) < cellWidth) rightClear = false
        if (Math.abs(this.y - i * cellWidth - cellWidth / 4) < cellWidth && Math.abs(j * cellWidth - this.x) < cellWidth) upClear = false
        if (Math.abs(i * cellWidth - this.y - cellWidth / 4) < cellWidth && Math.abs(j * cellWidth - this.x) < cellWidth) downClear = false
      }
    }
  }
  def spawnEnemy(maze: Array[Array[Int]]): Unit = {
    val r = scala.util.Random
    val newX = 5 + r.nextInt(maze.length - 6)
    val newY = 6 + r.nextInt(maze.length - 6)
    if (maze(newY)(newX) == 0) {
      this.mX = newX * cellWidth
      this.mY = newY * cellWidth
    } else {
      spawnEnemy(maze)
    }
  }
  val bWidth = 4
  def checkDead(bullets: Array[Passables.PassableItem]): Unit = {
    var flag = false
    for (b <- 0 until bullets.length) {
      if (bullets(b).i != 1 && Math.abs((bullets(b).x + bWidth / 2) - (this.x + cellWidth / 2)) < bWidth / 2 + cellWidth / 2 &&
        Math.abs((bullets(b).y + bWidth / 2) - (this.y + cellWidth / 2)) < bWidth / 2 + cellWidth / 2) {
        flag = true
        ServerMain.bulletHitEnemy(b)
      }
    }
    if (flag) {
      _alive = false
    }
  }
}


