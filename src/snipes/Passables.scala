

package snipes

object Passables {
  case class PassableCharacter(x:Int, y:Int, cType:Int)
  case class PassableItem(var i:Int, x:Int, y:Int, dir:Int, playerNum:Int)
  case class PassableLevel(style:Int, map:Array[Array[Int]], characters:Array[PassableCharacter], items:Array[PassableItem], enemies:Array[PassableCharacter])
  def element(x:Int,y:Int):MapElement.ElementType = ???
}