package snipes

import scala.reflect.ClassTag

class DLinkedList[A] extends Serializable {
  private var default: A = _

  private class Node(var prev: Node, val data: A, var next: Node) extends Serializable
  private val end = new Node(null, default, null)
  end.next = end
  end.prev = end
  var numElems = 0

  def add(elem: A, index: Int): Unit = {
    assert(index >= 0 && index <= numElems)
    var rover = end.next
    for (i <- 0 until index) rover = rover.next
    val n = new Node(rover.prev, elem, rover)
    rover.prev.next = n
    rover.prev = n
    numElems += 1
  }

  def remove(index: Int): A = {
    assert(index >= 0 && index <= numElems)
    var rover = end.next
    for (i <- 0 until index) rover = rover.next
    val tmp = rover.data
    rover.prev.next = rover.next
    rover.next.prev = rover.prev
    numElems -= 1
    tmp
  }

  def apply(index: Int): A = {
    assert(index >= 0 && index < numElems)
    var rover = end.next
    for (i <- 0 until index) rover = rover.next
    rover.data
  }

  def length: Int = {
    numElems
  }
  def toArray[B: ClassTag](f: A => B): Array[B] = {
    var rover = end.next
    Array.fill(length) {
      val temp = f(rover.data)
      rover = rover.next
      temp
    }
  }
  def changeAll(f:A=>A):Unit = {
    var rover = end.next
    for(i <- 0 until length) {
      f(rover.data)
      rover = rover.next
    }
  }
  
  def removeIf(f: A => Boolean): Unit = {
    var rover = end.next
    while (rover != end) {
      if(rover.next.data == default) {
        if(f(rover.data)) {
          end.next = end
          end.prev = end
          rover = end.next
          numElems -= 1
        }
      }
      else if (f(rover.data)) {
        rover.prev.next = rover.next
        rover.next.prev = rover.prev
        numElems -= 1
        rover = end.next
      } else {
        rover = rover.next
      }
    }
  }
}