
package snipes

import java.awt.image.BufferedImage
import java.rmi.Naming
import java.rmi.RemoteException
import java.rmi.server.UnicastRemoteObject

import scalafx.Includes.eventClosureWrapperWithParam
import scalafx.Includes.jfxKeyEvent2sfx
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.canvas.Canvas
import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.input.KeyCode
import scalafx.scene.input.KeyEvent
import scalafx.scene.paint.Color

@remote trait RemoteClient {
  @throws(classOf[RemoteException]) def updateLevel(pl: Passables.PassableLevel): Unit
  @throws(classOf[RemoteException]) def renderBullet(bullet: Passables.PassableItem): Unit
  @throws(classOf[RemoteException]) def newPoint: Unit
  @throws(classOf[RemoteException]) def changePlayerNum: Unit

}

object ClientMain extends UnicastRemoteObject with JFXApp with RemoteClient {
  val server = Naming.lookup("rmi://localhost:1099/ServerMain") match {
    case s: RemoteServer => s
    case _               => throw new RuntimeException("Invalid type for RMI server.")
  }
  var player: Int = server.connect(this)
  private var alive = true
  private var wonGame = false
  private var agile = true
  var currentLevel: Passables.PassableLevel = null
  private var numKills = 0
  var img: BufferedImage = null
  val canvas = new Canvas(512, 512)
  val gc = canvas.graphicsContext2D
  val playerSize = 16
  val cellWidth = 16
  var time = System.currentTimeMillis
  stage = new JFXApp.PrimaryStage {
    title = "Snipes"
    scene = new Scene(512, 512) {
      content = List(canvas)
      onKeyPressed = (e: KeyEvent) => {
        e.code match {
          case KeyCode.LEFT  => goLeft
          case KeyCode.RIGHT => goRight
          case KeyCode.UP    => goUp
          case KeyCode.DOWN  => goDown
          case KeyCode.W     => shoot(1)
          case KeyCode.S     => shoot(2)
          case KeyCode.A     => shoot(3)
          case KeyCode.D     => shoot(4)
          case _             =>
        }
      }
      var leftPressed = false
      var rightPressed = false
      var upPressed = false
      var downPressed = false
      private var rightClear: Boolean = true
      private var leftClear: Boolean = true
      private var upClear: Boolean = true
      private var downClear: Boolean = true
      val step = 4
      def goUp: Unit = {
        checkIntersect(currentLevel.characters(player), currentLevel)
        if (upClear && alive && agile) server.goUp(player)
      }
      def goDown: Unit = {
        checkIntersect(currentLevel.characters(player), currentLevel)
        if (downClear && alive && agile) server.goDown(player)
      }
      def goLeft: Unit = {
        checkIntersect(currentLevel.characters(player), currentLevel)
        if (leftClear && alive && agile) server.goLeft(player)
      }
      def goRight: Unit = {
        checkIntersect(currentLevel.characters(player), currentLevel)
        if (rightClear && alive && agile) server.goRight(player)
      }
      def checkIntersect(player: Passables.PassableCharacter, maze: Passables.PassableLevel): Unit = {
        clearAll
        for (i <- 0 until maze.map.length; j <- 0 until maze.map(0).length) {
          if (maze.map(i)(j) != 0) {
            if (Math.abs(player.x - j * cellWidth - step) < cellWidth && Math.abs(i * cellWidth - player.y) < cellWidth) leftClear = false
            if (Math.abs(j * cellWidth - player.x - step) < cellWidth && Math.abs(i * cellWidth - player.y) < cellWidth) rightClear = false
            if (Math.abs(player.y - i * cellWidth - cellWidth / 4) < cellWidth && Math.abs(j * cellWidth - player.x) < cellWidth) upClear = false
            if (Math.abs(i * cellWidth - player.y - cellWidth / 4) < cellWidth && Math.abs(j * cellWidth - player.x) < cellWidth) downClear = false
          }
        }
      }
      def clearAll(): Unit = {
        rightClear = true
        leftClear = true
        upClear = true
        downClear = true
      }
      def shoot(dir: Int): Unit = {
        if (System.currentTimeMillis - time > 700) {
          server.shoot(currentLevel.characters(player).x, currentLevel.characters(player).y, dir, player)
          time = System.currentTimeMillis
        }
      }
    }
  }
  def render(gc: GraphicsContext, level: Passables.PassableLevel): Unit = {
    val width = 512
    val length = 512
    val cellWidth = width / level.map.length
    val cellHeight = length / level.map.length
    gc.fill = Color.Black
    gc.fillRect(0, 0, width, length)
    for (i <- 0 until level.map.length; j <- 0 until level.map(0).length) {
      level.map(i)(j) match {
        case 0 =>
          gc.fill = Color.AliceBlue
          gc.fillRect(j * cellWidth, i * cellHeight, cellWidth, cellHeight)
        case 1 =>
          gc.fill = Color.Black
          gc.fillRect(j * cellWidth, i * cellHeight, cellHeight, cellWidth)
      }
    }
    drawItems(gc, level.items)
    drawCharacters(gc, level.characters)
    drawEnemies(gc, level.enemies)
    renderScore(gc)
    if (!alive) gameOver(gc)
    if (currentLevel.enemies.length < 1) {
      agile = false
      winner(gc)
    }
  }
  def drawCharacters(gc: GraphicsContext, characters: Array[Passables.PassableCharacter]): Unit = {
    for (i <- 0 until characters.length) {
      gc.fill = Color.DarkViolet
      gc.fillRect(characters(i).x, characters(i).y, cellWidth, cellWidth)
    }
  }
  def drawItems(gc: GraphicsContext, items: Array[Passables.PassableItem]): Unit = {
    for (i <- 0 until items.length) {
      if (items(i).i == 0 && !checkBulletIntersect(items(i), i)) {
        gc.fill = Color.Red
        gc.fillRect(items(i).x, items(i).y, 4, 4)
      }
    }
  }
  def renderScore(gc: GraphicsContext) = {
    val score = "Score: " + numKills
    gc.fill = Color.White
    gc.fillText(score, 220, 12)
  }
  def drawEnemies(gc: GraphicsContext, enemies: Array[Passables.PassableCharacter]): Unit = {
    for (i <- 0 until enemies.length) {
      gc.fill = Color.Orange
      gc.fillRect(enemies(i).x, enemies(i).y, cellWidth, cellWidth)
    }
  }
  def gameOver(gc: GraphicsContext): Unit = {
    val text = "GAME OVER"
    gc.fill = Color.Red
    gc.fillText(text, 205, 30)
  }
  def winner(gc: GraphicsContext): Unit = {
    val text = "YOU WIN!!!"
    gc.fill = Color.Green
    gc.fillText(text, 210, 30)
  }
  def checkDead: Unit = {
    for (e <- 0 until currentLevel.enemies.length) {
      if (Math.abs((currentLevel.enemies(e).x + cellWidth / 2) - (currentLevel.characters(player).x + cellWidth / 2)) < cellWidth &&
        Math.abs((currentLevel.enemies(e).y + cellWidth / 2) - (currentLevel.characters(player).y + cellWidth / 2)) < cellWidth) {
        alive = false
        server.playerDead(player)
      }
    }
  }
  @throws(classOf[RemoteException]) def renderBullet(bullet: Passables.PassableItem): Unit = {
    gc.fill = Color.Cyan
    gc.fillRect(bullet.x, bullet.y, 4, 4)
  }
  @throws(classOf[RemoteException]) def updateLevel(pl: Passables.PassableLevel): Unit = {
    currentLevel = pl
    checkDead
    if (gc != null) render(gc, currentLevel)
  }
  @throws(classOf[RemoteException]) def newPoint: Unit = {
    numKills += 1
  }
  @throws(classOf[RemoteException]) def changePlayerNum: Unit = {
    player -= 1
  }


  val bSpeed = 8
  val bWidth = 4
  def checkBulletIntersect(bullet: Passables.PassableItem, num: Int): Boolean = {
    val maze = currentLevel.map
    var removeItems = List[Int]()
    var flag = false
    for (i <- 0 until maze.length; j <- 0 until maze(0).length) {
      if (maze(i)(j) != 0) {
        if (Math.abs((bullet.x + bWidth / 2) - (j * cellWidth + cellWidth / 2)) < bWidth / 2 + cellWidth / 2 && Math.abs((bullet.y + bWidth / 2) - (i * cellWidth + cellWidth / 2)) < bWidth / 2 + cellWidth / 2) flag = true
      }
    }
    if (flag) server.removeBullet(num)
    flag
  }
}
