

package snipes

import java.rmi.server.UnicastRemoteObject


class Player(val client:RemoteClient, private var mX:Int, private var mY:Int) extends UnicastRemoteObject with RemotePlayer with Character  {
  def cType = {
    0
  }
  def x:Int = mX
  def y:Int = mY
  def passable:Passables.PassableCharacter = Passables.PassableCharacter(this.mX, this.mY, cType) 
  def move(dx:Int, dy:Int) = {
    mX += dx
    mY += dy
  }
  def update:Unit = ???
  def createPassable:Passables.PassableCharacter = ???
}


