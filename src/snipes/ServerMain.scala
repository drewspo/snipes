package snipes

import java.rmi.server.UnicastRemoteObject
import java.rmi.RemoteException
import java.rmi.Remote
import collection.mutable
import java.awt.image.BufferedImage
import java.rmi.Naming
import java.rmi.registry.LocateRegistry
import collection.mutable.Buffer

@remote trait RemoteServer {
  @throws(classOf[RemoteException]) def connect(client: RemoteClient): Int
  @throws(classOf[RemoteException]) def goUp(player: Int)
  @throws(classOf[RemoteException]) def goDown(player: Int)
  @throws(classOf[RemoteException]) def goLeft(player: Int)
  @throws(classOf[RemoteException]) def goRight(player: Int)
  @throws(classOf[RemoteException]) def shoot(x: Int, y: Int, dir: Int, playerNum: Int)
  @throws(classOf[RemoteException]) def removeBullet(bulletNum: Int): Unit
  @throws(classOf[RemoteException]) def playerDead(playerNum: Int): Unit

}

object ServerMain extends UnicastRemoteObject with RemoteServer {
  private val players = new DLinkedList[Player]()
  private val items = new DLinkedList[Item]()
  private var playing = true
  private var currentLevel: Level = {
    GameEngine.levels(0)
  }
  def main(args: Array[String]): Unit = {
    for (i <- 1 to 15) {
      val newEnemy = new Enemy(1, 24, 24)
      newEnemy.changeNum(currentLevel.enemies.length)
      newEnemy.spawnEnemy(currentLevel.map)
      currentLevel.enemies.add(newEnemy, currentLevel.characters.length)
    }
    LocateRegistry.createRegistry(1099)
    Naming.rebind("ServerMain", this)
    while (playing) {
      moveBullets
      moveEnemies
      val pl = currentLevel.passable
      for (c <- 0 until players.length) {
        players(c).client.updateLevel(pl)
      }
      for (c <- removePlayers) {
        players.remove(c)
        currentLevel.characters.remove(c)
      }
      if (removePlayers.length > 0) {
        for (c <- 0 until currentLevel.characters.length; if removePlayers.max <= c) {
          players(c).client.changePlayerNum
        }
      }
      removePlayers = List[Int]()
      if (currentLevel.enemies.length < 1) endGame
      Thread.sleep(50)
    }
  }
  @throws(classOf[RemoteException]) def connect(client: RemoteClient): Int = {
    players synchronized {
      val newPlayer = new Player(client, 16, 16)
      players.add(newPlayer, players.length)
      currentLevel.addCharacter(newPlayer)
      players.length - 1
    }
  }
  val step = 4
  val backstep = -4
  val bSpeed = 8
  @throws(classOf[RemoteException]) def goUp(player: Int) = {
    players(player).move(0, step * (-1))
  }
  @throws(classOf[RemoteException]) def goDown(player: Int) = {
    players(player).move(0, step)
  }
  @throws(classOf[RemoteException]) def goLeft(player: Int) = {
    players(player).move(-1 * step, 0)
  }
  @throws(classOf[RemoteException]) def goRight(player: Int) = {
    players(player).move(step, 0)
  }
  @throws(classOf[RemoteException]) def shoot(x: Int, y: Int, dir: Int, playerNum: Int) = {
    currentLevel.addItem(new Item(0, x + 8, y + 8, dir, playerNum))
  }
  @throws(classOf[RemoteException]) def removeBullet(bulletNum: Int): Unit = {
    currentLevel.items(bulletNum).i = 1
  }
  private var removePlayers = List[Int]()
  @throws(classOf[RemoteException]) def playerDead(playerNum: Int): Unit = {
    removePlayers = playerNum :: removePlayers
  }
  def moveBullets = {
    var rover = 0
    while (rover < currentLevel.items.length) {
      currentLevel.items(rover).direction match {
        case 1 => {
          currentLevel.items(rover).move(0, bSpeed * (-1))
        }
        case 2 => {
          currentLevel.items(rover).move(0, bSpeed)
        }
        case 3 => {
          currentLevel.items(rover).move(bSpeed * (-1), 0)
        }
        case 4 => {
          currentLevel.items(rover).move(bSpeed, 0)
        }
      }
      rover += 1
    }
  }
  def moveEnemies = {
    val removeTheseEnemies = Buffer[Int]()
    for (i <- 0 until currentLevel.enemies.length) {
      val e = currentLevel.enemies(i).moveEnemy(currentLevel.map, currentLevel.passable.items)
      if (e == true) {
        removeTheseEnemies += i
      }
    }
    if (removeTheseEnemies.length > 0) {
      for (i <- 0 until removeTheseEnemies.length) {
        currentLevel.enemies.remove(removeTheseEnemies(i))
      }
    }
  }
  def bulletHitEnemy(bullet: Int) = {
    currentLevel.items(bullet).i = 1
    if (players.length > 0) players(currentLevel.items(bullet).pNum).client.newPoint
  }
  def endGame: Unit = {
    for (c <- players.length - 1 to 0) {
      players.remove(c)
      currentLevel.characters.remove(c)
      playing = false
    }
  }
} 