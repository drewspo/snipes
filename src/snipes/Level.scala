

package snipes

class Level(style:Int, private var elems:Array[Array[Int]], private var mChars:DLinkedList[Character], var items:DLinkedList[Item], var enemies:DLinkedList[Enemy]) {
  def map = elems
  def passable = {
    new Passables.PassableLevel(this.style, this.elems, mChars.toArray(_.passable), items.toArray(_.passable), enemies.toArray(_.passable))
  }
  def element(x:Int, y:Int):MapElement.ElementType = ???
  def addCharacter(c:Character):Unit = {
    mChars.add(c, mChars.length)
  }
  def addItem(item:Item):Unit = {
    items.add(item, items.length)
  }
  def removeCharacter(c:Character):Unit = ???
  def characters:DLinkedList[Character] = mChars
}
  



