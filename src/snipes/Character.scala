

package snipes


trait Character {
  def x:Int 
  def y:Int 
  def cType:Int
  def move(mX:Int, mY:Int):Unit 
  def update:Unit 
  def passable:Passables.PassableCharacter
}

object Character extends Enumeration {
  type CharacterType = Value
  val Player, Enemy = Value
}



