

package snipes

class Item(var i:Int, private var x:Int, private var y:Int, private var dir:Int, private var playerNum:Int) {
  def pNum = playerNum
  def createPassable:Passables.PassableItem = {
    new Passables.PassableItem(i, x, y, dir, playerNum)
  }
  def passable:Passables.PassableItem = Passables.PassableItem(this.i, this.x, this.y, dir, playerNum)
  def move(dx:Int, dy:Int) = {
    x += dx
    y += dy
  }
  def mX = x
  def mY = y
  def direction = dir
}