

package snipes

import java.rmi.server.UnicastRemoteObject

class NewPlayer extends UnicastRemoteObject  {
  def x:Int = ???
  def y:Int = ??? 
  def level:Level = ??? 
  def move(dx:Int, dy:Int):Unit = ??? 
  def update:Unit = ???
  def createPassable:Passables.PassableCharacter = ???
}


